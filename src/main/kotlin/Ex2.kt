/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: EmployeeById
 */

import java.util.Scanner

private val sc = Scanner(System.`in`)

data class Employee(val dni: String, val name: String, val lastName: String, val address: String)

fun main(){
    println("Enter how many employees you would like to register:")
    var num = sc.nextInt()
    val employees = registerEmployee(num)
    checkEmployee(employees)
}

fun registerEmployee(num: Int): MutableMap<String,Employee> {
    sc.nextLine()
    val employeeMap = mutableMapOf<String,Employee>()

    for (i in 1..num){
        println("Enter the employee's DNI:")
        val dniE = sc.nextLine()
        println("Enter the employee's name:")
        val nameE = sc.nextLine()
        println("Enter the employee's lastname:")
        val lastnameE = sc.nextLine()
        println("Enter the employee's address:")
        val addressE = sc.nextLine()

        employeeMap.put(dniE, Employee(dniE,nameE,lastnameE,addressE))
    }

    return employeeMap
}

fun checkEmployee(employees: MutableMap<String,Employee>){
    var dniCheck = ""

    while (true){
        dniCheck = sc.next()
        if(dniCheck.uppercase()=="END")break
        println("${employees[dniCheck]?.name?.capitalize()} " +
                "${employees[dniCheck]?.lastName?.capitalize()}" +
                " - ${employees[dniCheck]?.dni}," +
                " ${employees[dniCheck]?.address}")
    }
}
