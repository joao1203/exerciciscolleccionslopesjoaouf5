/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/15
* TITLE: RoadSigns
 */

import java.util.Scanner

private val sc = Scanner(System.`in`)

fun main(){
    println("Enter the amount of signs you would like to add:")
    var num = sc.nextInt()
    val signs = amountSigns(num)
    println("Enter the amount of checks you'd like to realize:")
    num = sc.nextInt()
    checkSigns(num,signs)
}
fun amountSigns(num: Int): MutableMap<Int,String> {
    val signs= mutableMapOf<Int,String>()

    for (i in 1..num){
        println("Sign ${i}: Enter the meter and what the sign should say:")
        val meterSign = sc.nextInt()
        val textSign = sc.next()
        signs.put(meterSign,textSign)
    }

    return signs
}

fun checkSigns(num: Int, signs: MutableMap<Int,String>){
    for (i in 1..num){
        println("Enter the meter where the sign is located:")
        val check = sc.nextInt()
        if (check in signs){
            println("Sign located in $check meters: ${signs[check]?.capitalize()}")
        }
        else {
            println("There is no sign!")
        }
    }
}
