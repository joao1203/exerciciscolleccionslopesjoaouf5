/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: RepeatedAnswer
 */

import java.util.*

fun main() {
    val sc = Scanner(System.`in`)
    val wordList= mutableSetOf<String>()
    var word = ""

    while (word.uppercase()!="END"){
        word = sc.next()
        if(wordList.add(word)){
            wordList.add(word)
        }
        else println("MEEEC!")
    }
}

