/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: Bingo
 */

import java.util.*

private val sc = Scanner(System.`in`)

fun main(){
    println("Enter the numbers of the bingo chart:")
    val bingoChart = mutableSetOf<Int>()
    for (i in 1..10){
        val num = sc.nextInt()
        bingoChart.add(num)
    }
    bingo(bingoChart)
}

fun bingo(bingoChart: MutableSet<Int>){
    println("Bingo:")
    do{
        val num = sc.nextInt()
        if (num in bingoChart){
            bingoChart.remove(num)
            println("There are ${bingoChart.size} numbers left!")
        }
        else {
            println("There are ${bingoChart.size} numbers left!")
        }
    }while (bingoChart.size != 0)
    println("BINGO!")
}
