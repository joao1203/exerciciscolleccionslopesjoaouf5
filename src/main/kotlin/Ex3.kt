/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/16
* TITLE: SchoolDelegatVoting
 */

import java.util.Scanner

fun main(){
    for (i in votingDelegat()){
        println("${i.key}: ${i.value}")
    }
}
fun votingDelegat(): MutableMap<String,Int>{
    val sc = Scanner(System.`in`)
    var endVote = false
    val electionVotes= mutableMapOf<String, Int>()

    while (!endVote){
        println("Who are you voting for?")
        val vote = sc.next().uppercase()

        if (vote in electionVotes){
            electionVotes[vote] = electionVotes[vote]!! + 1
        }
        else if(vote.uppercase() == "END"){
            endVote = true
        }
        else {
            electionVotes.put(vote, 1)
        }
    }
    return electionVotes
}
